#!/bin/bash

KUBEVERSION=$1

echo start of installing $0

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system

sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y kubelet=$1 kubeadm=$1 kubectl=$1
sudo apt-mark hold kubelet kubeadm kubectl

sed -i '/kind: KubeletConfiguration/ a cgroupDriver: systemd' /var/lib/kubelet/config.yaml

systemctl daemon-reload
systemctl restart kubelet

echo "kubeadm init --pod-network-cidr=10.244.0.0/16"
kubeadm init --pod-network-cidr=10.244.0.0/16 --cri-socket=/var/run/crio/crio.sock
kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml

echo end of installing $0
